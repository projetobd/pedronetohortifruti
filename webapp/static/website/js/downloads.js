function createPDF_insert_pedido() {
	var doc = new jsPDF('a4');

	doc.text(10, 10, 'Pedro Neto Hortifruti Granjeiro');

	doc.setFont("times");
	doc.setFontType("italic");

	doc.text(10, 30, 'Nome');
	doc.text(110, 30, 'Preço');
	doc.text(130, 30, 'UND');
	doc.text(150, 30, 'QTD');
	doc.text(165, 30, 'CAT');
	doc.text(180, 30, 'Total');

	var p = 0;
	$('#table2').find('tbody').eq(0).find('tr').each(function (i, v) {
		if (!$(v).is(':hidden')) {
			var cols = [];
			nome = $(v).find('.Nome').text();
			preco = $(v).find('.Preco').val();
			unidade = $(v).find('.Unidade').text();
			quantidade = $(v).find('.QTD').val();
			categoria = $(v).find('.Category').text();
			total = $(v).find('.Total').val();

			pageHeight= doc.internal.pageSize.height;

			if (40 + 15*p>=pageHeight) {
				pdf.addPage();
			}

			doc.text(10, 40 + 15*p, nome);
			doc.text(110, 40 + 15*p, preco);
			doc.text(130, 40 + 15*p, unidade);
			doc.text(150, 40 + 15*p, quantidade);
			doc.text(165, 40 + 15*p, categoria);
			doc.text(180, 40 + 15*p, total);

			p++;
		}
	});
	doc.text(170, 40 + 15*p, 'Total: ' + $('#valor_total').val());
	doc.text(10, 40 + 15*p,  'Cliente: ' + $('.table-usuarios-selected').text());


	doc.save('produto.pdf'); // Save the PDF with name "katara"...  
}

function viewPDF_insert_pedido() {
	var doc = new jsPDF('a4');

	doc.text(10, 10, 'Pedro Neto Hortifruti Granjeiro');

	doc.setFont("times");
	doc.setFontType("italic");

	doc.text(10, 30, 'Nome');
	doc.text(110, 30, 'Preço');
	doc.text(130, 30, 'UND');
	doc.text(150, 30, 'QTD');
	doc.text(165, 30, 'CAT');
	doc.text(180, 30, 'Total');

	var p = 0;
	$('#table2').find('tbody').eq(0).find('tr').each(function (i, v) {
		if (!$(v).is(':hidden')) {
			var cols = [];
			nome = $(v).find('.Nome').text();
			preco = $(v).find('.Preco').val();
			unidade = $(v).find('.Unidade').text();
			quantidade = $(v).find('.QTD').val();
			categoria = $(v).find('.Category').text();
			total = $(v).find('.Total').val();

			pageHeight= doc.internal.pageSize.height;

			if (40 + 15*p>=pageHeight) {
				pdf.addPage();
			}

			doc.text(10, 40 + 15*p, nome);
			doc.text(110, 40 + 15*p, preco);
			doc.text(130, 40 + 15*p, unidade);
			doc.text(150, 40 + 15*p, quantidade);
			doc.text(165, 40 + 15*p, categoria);
			doc.text(180, 40 + 15*p, total);

			p++;
		}
	});
	doc.text(170, 40 + 15*p, 'Total: ' + $('#valor_total').val());
	doc.text(10, 40 + 15*p,  'Cliente: ' + $('.table-usuarios-selected').text());

	doc.output('dataurlnewwindow');

} 
