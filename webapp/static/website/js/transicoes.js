var animando = false;
var valTabAtual = 0;
var valPagAtual = 0;
var $pages;
var $botoes;
var endPagAtual = false;
var endProxPagina = false;

var animEndEventNames = {
	'WebkitAnimation': 'webkitAnimationEnd',
	'OAnimation': 'oAnimationEnd',
	'msAnimation': 'MSAnimationEnd',
	'animation': 'animationend'
};
// animation end event name
var animEndEventName = animEndEventNames[ Modernizr.prefixed('animation') ];

// support css animations
var support = Modernizr.cssanimations;

var init_transicoes = function (conteudo, botoes) {
	$pages = conteudo;
    $botoes = botoes;
    $botoes.eq(valTabAtual).addClass('tab-current');
    $pages.eq(valPagAtual).addClass('pagina_atual');
};

var animcursorCheck = function () {
	if (animando) {
	return false;
	}
	else {
	return true;
	}
};

var mudaPagina = function (proximoPag, proximoTab) {

	proximoTab = proximoTab || proximoPag;

	if (animando) {
		return false;
	}
	if (proximoPag == valPagAtual) {
		return false;
	}

	animando = true;

	var $pagAtual = $pages.eq(valPagAtual);
    var $tabAtual = $botoes.eq(valTabAtual);

	valPagAtual = proximoPag;
	valTabAtual = proximoTab;

    var $proxPagina = $pages.eq(valPagAtual).addClass('pagina_atual');

    var outClass = 'pt-page-moveToTop';
    var inClass = 'pt-page-moveFromBottom';

    $tabAtual.attr('class', 'tab');
    $botoes.eq(valTabAtual).attr('class', 'tab tab-current');
    
	$pagAtual.addClass(outClass).on(animEndEventName, function () {
	$pagAtual.off(animEndEventName);
	endPagAtual = true;
	if (endProxPagina) {
		onEndAnimation($pagAtual, $proxPagina);
	}
	});

	$proxPagina.addClass(inClass).on(animEndEventName, function () {
	$proxPagina.off(animEndEventName);
	endProxPagina = true;
	if (endPagAtual) {
		onEndAnimation($pagAtual, $proxPagina);
	}
	});

	if (!support) {
	    onEndAnimation($pagAtual, $proxPagina);
	}
};

function onEndAnimation($outpage, $inpage) {
	endPagAtual = false;
	endProxPagina = false;
	resetPage($outpage, $inpage);
	animando = false;
}

function resetPage($outpage, $inpage) {
	$outpage.attr('class', 'pagina');
	$inpage.attr('class', 'pagina pagina_atual');
}
