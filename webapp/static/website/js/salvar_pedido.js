$(document).ready(function () {

    //noinspection JSUnusedLocalSymbols
    insere_pedido = function (user, produtos, preco, qtd, csrf, callback) {
        jQuery.ajax({
            type: "POST",
            cache: false,
            data: { action: 'insert_pedido', produtos: JSON.stringify(produtos), user: JSON.stringify(user), preco: JSON.stringify(preco), qtd: JSON.stringify(qtd), csrfmiddlewaretoken: csrf },
            success: function (data) {
                callback();
            },
            error: function (xhr, errmsg, err) {
                alert(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            }
        });
    };

    //noinspection JSUnusedLocalSymbols
    salva_pedido = function (csrf, callback) {
        jQuery.ajax({
            type: "POST",
            cache: false,
            data: { action: 'query', tipodebusca: tipodebusca, query_string: query_string, csrfmiddlewaretoken: csrf },
            success: function (data) {
                jQueryData = jQuery.parseJSON(data);
                callback(jQueryData);
            },
            error: function (xhr, errmsg, err) {
                alert(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            }
        });
    };

});

