$(document).ready(function () {

    $("#id_q").val("");

    //noinspection JSUnusedLocalSymbols
    buscar = function (tipodebusca, query_string, callback) {
        jQuery.ajax({
            type: "GET",
            cache: false,
            data: { action: 'query', tipodebusca: tipodebusca, query_string: query_string },
            success: function (data) {
		        //console.log(data);
                jQueryData = jQuery.parseJSON(data);
                //console.log(jQueryData);
                callback(jQueryData);
            },
            error: function (xhr, errmsg, err) {
                alert(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            }
        });
    };

});
