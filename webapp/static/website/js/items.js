jQuery(document).ready(function($){
	var FinalWidth = 300,
        maxQuickWidth = 700;

    //Prepara o abrir_view atribuindo uma função ao evento de click dele.
    //A função 'f' deve conter qualquer tratamento específico que deva ocorrer quando
    //o view for abrir
    preparar_abrir_view = function (obj, f) {
        obj.click( function(ev) {
            f(obj);
        });
	};

    //Abrir o view genérico, para todos os casos. É bom ser chamada antes!
    abrir_view_generico = function() {
        $('body').addClass('overlay-layer');
        animateQuickView('open');
    };

    //Para abrir a view de um item, ou seja, não usar pra botões e etc
    abrir_view_item = function(obj) {

        var image = obj.find('img');

        obj.addClass('empty-box');

        //Isso aqui guarda os tamanhos iniciais do objeto original, etc
        //Pra fazer a animação!

        var parentListItem = obj,
            topSelected = image.offset().top,
            leftSelected = image.offset().left,
            widthSelected = image.width(),
            heightSelected = image.height(),
            windowWidth = $("#conteudo").width(),
            windowHeight = $("#conteudo").height(),
            finalLeft = (windowWidth - FinalWidth)/2,
            finalHeight = 400,
            finalTop = (windowHeight - 100) / 2,
            quickViewWidth = maxQuickWidth,
            quickViewLeft = (windowWidth - quickViewWidth)/2;

        $('.view').data({
            'parentListItem': parentListItem,
            'topSelected': topSelected,
            'leftSelected': leftSelected,
            'widthSelected': widthSelected,
            'heightSelected': heightSelected,
            'windowWidth': windowWidth,
            'windowHeight': windowHeight,
            'finalLeft': finalLeft,
            'finalHeight': finalHeight,
            'finalTop': finalTop,
            'quickViewWidth': quickViewWidth,
            'quickViewLeft': quickViewLeft
        });

    };

	//close the quick view panel
	$('body').on('click', function(event){
		if( $(event.target).is('.fechar-view') || $(event.target).is('body.overlay-layer')) {
			closeQuickView( FinalWidth, maxQuickWidth);
		}
	});
	$(document).keyup(function(event){
		//check if user has pressed 'Esc'
    	if(event.which=='27'){
			closeQuickView( FinalWidth, maxQuickWidth);
		}
	});


	function closeQuickView(finalWidth, maxQuickWidth) {
		var close = $('.fechar-view'),
			activeSliderUrl = close.siblings('.view-imagem-grande').find('.selected img').attr('src'),
			selectedImage = $('.empty-box').find('img');

		if( !$('.view').hasClass('velocity-animating') && $('.view').hasClass('view-mostra-conteudo')) {
			selectedImage.attr('src', activeSliderUrl);
			animateQuickView('close');
		} else {
			closeNoAnimation(selectedImage, finalWidth, maxQuickWidth);
		}
	}

	function animateQuickView(animationType) {
        view = $('.view');

        var	parentListItem = view.data('parentListItem'),
            topSelected = view.data('topSelected'),
            leftSelected = view.data('leftSelected'),
            widthSelected = view.data('widthSelected'),
            heightSelected = view.data('heightSelected'),
            windowWidth = view.data('windowWidth'),
            windowHeight = view.data('windowHeight'),
            finalLeft = view.data('finalLeft'),
            finalHeight = view.data('finalHeight'),
            finalTop = view.data('finalTop'),
            quickViewWidth = view.data('quickViewWidth'),
            quickViewLeft = view.data('quickViewLeft');

        var finalWidth = FinalWidth;

        if (view.data('quickViewWidth')) {
            finalWidth = view.data('quickViewWidth');
        }

		if( animationType == 'open') {
			//place the quick view over the image gallery and give it the dimension of the gallery image
			$('.view').css({
			    "top": topSelected,
			    "left": leftSelected,
			    "width": widthSelected,
				"opacity": 0
			}).velocity({
				//animate the quick view: animate its width and center it in the viewport
				//during this animation, only the slider image is visible
			    'top': finalTop+ 'px',
			    'left': finalLeft+'px',
			    'width': finalWidth+'px',
                'height': finalHeight+'px',
				'opacity': 1
			}, 500, [ 40, 20 ], function(){
				//animate the quick view: animate its width to the final value
				$('.view').addClass('animate-width').velocity({
					'left': quickViewLeft+'px',
			    	'width': quickViewWidth+'px',
				}, 300, 'ease' ,function(){
					//show quick view content
					$('.view').addClass('view-mostra-conteudo');
				});
			}).addClass('is-visible');

		} else {

			//close the quick view reverting the animation
			view.removeClass('view-mostra-conteudo').velocity({
			    'top': finalTop+ 'px',
			    'left': finalLeft+'px',
			    'width': finalWidth+'px',
				'height': finalHeight+'px',
				'opacity': 1
			}, 300, 'ease', function(){
				$('body').removeClass('overlay-layer');
				$('.view').removeClass('animate-width').velocity({
					"top": topSelected,
				    "left": leftSelected,
				    "width": widthSelected,
					"height": heightSelected,
					"opacity": parentListItem ? 1 : 0
				}, 500, [ 40, 20 ], function(){
					$('.view').removeClass('is-visible');
					if(parentListItem) {
						parentListItem.removeClass('empty-box');
					}

				var cd_quick_view = $('.view');

            	cd_quick_view.find('div').remove();
            	cd_quick_view.css({left: "", top: "", width: "", height: "", opacity: "" }); //gambiarra pra funcionar no IE
				});
			});
			view.removeData();
		}

	}
	function closeNoAnimation(image, finalWidth, maxQuickWidth) {
		var parentListItem = image.parent('.cd-item'),
			topSelected = image.offset().top - $(this).parent().scrollTop(),
			leftSelected = image.offset().left,
			widthSelected = image.width();

		//close the quick view reverting the animation
		$('body').removeClass('overlay-layer');
		parentListItem.removeClass('empty-box');
		$('.view').velocity("stop").removeClass('view-mostra-conteudo animate-width is-visible').css({
			"top": topSelected,
		    "left": leftSelected,
		    "width": widthSelected,
		});
	}
});

