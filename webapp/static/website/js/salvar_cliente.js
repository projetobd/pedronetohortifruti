$(document).ready(function () {

    //noinspection JSUnusedLocalSymbols
    insere_cliente = function (first_name, last_name, email, login, ativo, ddd, numero, ddd2, numero2, rua, numero_endereco, bairro, cidade, estado, cep, complemento, csrf, callback) {
        jQuery.ajax({
            type: "POST",
            cache: false,
            data: {
                action: 'insert_cliente',
                csrfmiddlewaretoken: csrf,
                first_name: first_name,
                last_name: last_name,
                email: email,
                login: login,
                ativo: ativo,
                ddd: ddd,
                numero: numero,
                ddd2: ddd2,
                numero2: numero2,
                rua: rua,
                numero_endereco: numero_endereco,
                bairro: bairro,
                cidade: cidade,
                estado: estado,
                cep: cep,
                complemento: complemento
            },
            success: function (data) {
                callback();
            },
            error: function (xhr, errmsg, err) {
                alert(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            }
        });
    };

    //noinspection JSUnusedLocalSymbols
    salva_cliente = function (csrf, callback) {
        jQuery.ajax({
            type: "POST",
            cache: false,
            data: { action: 'query', tipodebusca: tipodebusca, query_string: query_string, csrfmiddlewaretoken: csrf },
            success: function (data) {
                jQueryData = jQuery.parseJSON(data);
                callback(jQueryData);
            },
            error: function (xhr, errmsg, err) {
                alert(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            }
        });
    };

});

