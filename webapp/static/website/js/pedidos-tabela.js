$(document).ready(function() {
    var calculaValorTotal = function () {
        var valor = 0.0;
        $('#table2').find('tbody').eq(0).find('tr').each(function (i, v) {
            if (!$(v).is(':hidden')) {
                v_l = $(v).find('.Total').val();
                if (v_l) {
                    valor += parseFloat(v_l);
                }
            }
        });

        $("#valor_total").val(valor);
    };

    var showRow = function (row) {
        if (row.hasClass('showAppend') && row.hasClass('showQuery')) {
            row.show();
        } else {
            row.hide();
        }
    };
    var qs, qs_user;
    init_searches = function () {
        add_produtos_tabelas = function (produtos) {
            $.each(produtos, function (chave, val) {
                if(val.disponivel) {
                    append_dual_table(
                        val.id,
                        '<span class="Nome">' + val.nome + '</span>',
                        '<input class="Preco" value="' + val.preco + '" step="0.01" type="number" min="0"/>',
                        '<span class="Unidade">' + val.unidade + '</span>',
                        '<input class="QTD" step="' + val.granularidade + '" type="number" min="' + val.compra_minima + '"/>',
                        '<span class="Category">' + val.category + '</span>',
                        '<input class="Total" type="number" disabled/>'
                    );
                }

            });

        };

        add_clientes_tabela = function(clientes) {
            $.each(clientes, function (chave, val) {
                append_user(val.user, val.first_name + ' ' + val.last_name);

            });
        }

        //Faz a busca no BD dos produtos
        buscar('produto', "", add_produtos_tabelas);

        //Faz a busca no BD dos clientes
        buscar('cliente', "", add_clientes_tabela);


        qs = $('#search_produto1').quicksearch('table#table1 tbody tr', {
            show: function () {
                $(this).addClass('showQuery');
                showRow($(this));
            },
            hide: function () {
                $(this).removeClass('showQuery');
                showRow($(this));
            }
        });

        qs_user = $('#search_user').quicksearch('table#table_usuarios tbody tr');
    };

    var append_user = function(id, nome) {
        var table = $('#table_usuarios').find('tbody').eq(0);

        var row = $('#user-row-template').html();
        var row_Compilado = row.replace(/__nome__/g, nome);

        var slide = function() {
            var that = $(this);
            var this_indent = 0;

            if( (this.offsetHeight < this.scrollHeight) || (this.offsetWidth < this.scrollWidth) ) {
                interval = setInterval(function(){
                    this_indent--;
                    if(this_indent < -250) {
                        this_indent = 100;
                    }
                    $(that).css('text-indent', this_indent);
                },20);

                $(this).data("interval",interval);
            }
        };
        var slideout = function() {
            clearInterval($(this).data("interval"));
            $(this).css("text-indent",0);
        };

        row = $(row_Compilado);

        row.data({
            'id': id
        });

        row.find('td').eq(0).on("mouseenter",slide);
        row.find('td').eq(0).on("mouseleave",slideout);

        table.append(row);

        bot = row.find(".div-table-bot").eq(0);

        bot.click(function(e) {
            if(row.hasClass('table-usuarios-selected')) {
                row.removeClass('table-usuarios-selected')
            } else {
                var table = $('#table_usuarios').find('tbody').eq(0);

                table.find('tr').each(function(i, v) {
                    if($(v).hasClass('table-usuarios-selected')) {
                        $(v).removeClass('table-usuarios-selected');
                    }
                });

                row.addClass('table-usuarios-selected');
            }
        });

        qs_user.cache();
    };

    var append_dual_table = function(id) {
        var table1 = $('#table1').find('tbody').eq(0);
        var table2 = $('#table2').find('tbody').eq(0);

        var row = $('#row-template').html();

        var cols = "";

        var col = $('#col-obj').html();

        for (var i = 1; i < arguments.length; i++) {
            var col_Compilado = col.replace(/__obj__/g, arguments[i]);

            cols += col_Compilado;
        }
        var col_Compilado1 = col.replace(/__obj__/g, '<div class="div-table-bot"></div>');
        var col_Compilado2 = col.replace(/__obj__/g, '<div class="div-table-bot"></div>');
        var cols1 = cols + col_Compilado1;
        var cols2 = cols + col_Compilado2;

        var row1 = $(row.replace(/__colunas__/g, cols1));
        var row2 = $(row.replace(/__colunas__/g, cols2));

        var slide = function() {
            var that = $(this);
            var this_indent = 0;

            if( (this.offsetHeight < this.scrollHeight) || (this.offsetWidth < this.scrollWidth) ) {
                interval = setInterval(function(){
                    this_indent--;
                    if(this_indent < -250) {
                        this_indent = 100;
                    }
                    $(that).css('text-indent', this_indent);
                },20);

                $(this).data("interval",interval);
            }
        };
        var slideout = function() {
            clearInterval($(this).data("interval"));
            $(this).css("text-indent",0);
        };

        row2.data({
            'id': id
        });

        row1.find('td').eq(0).on("mouseenter",slide);
        row1.find('td').eq(0).on("mouseleave",slideout);
        row2.find('td').eq(0).on("mouseenter",slide);
        row2.find('td').eq(0).on("mouseleave",slideout);

        var qtdedit = function() {
            preco = $(this).parent().parent().find('.Preco').eq(0).val();
            qtd = $(this).parent().parent().find('.QTD').eq(0).val();
            $(this).parent().parent().find('.Total').eq(0).val(preco*qtd);
            calculaValorTotal();
        };

        row1.find('input').on('input', qtdedit);
        row2.find('input').on('input', qtdedit);

        bot1 = row1.find(".div-table-bot").eq(0);
        bot2 = row2.find(".div-table-bot").eq(0);

        table1.append(row1);
        table2.append(row2);

        row2.hide();

        bot1.click(function(e) {
            row2.show();

            row2.find('input').each(function(i, v) {
                $(v).eq(0).val(
                    row1.find(
                        '.' + $(v).eq(0).attr('class')
                    ).val()
                );
            });

            row1.removeClass('showAppend');
            showRow(row1);

            calculaValorTotal();
        });

        bot2.click(function() {
            row1.addClass('showAppend');
            showRow(row1);

            row1.find('input').each(function(i, v) {
                $(v).eq(0).val(
                    row2.find(
                        '.' + $(v).eq(0).attr('class')
                    ).val()
                );
            });

            row2.hide();

            calculaValorTotal();
        });

        qs.cache();
    };

});