$(document).ready(function () {

    //noinspection JSUnusedLocalSymbols
    insere_produto = function (nome, preco, unidade, disponivel, category, compra_minima, granularidade, csrf, callback) {
        jQuery.ajax({
            type: "POST",
            cache: false,
            data: { action: 'insert_produto', nome: nome, preco: preco, unidade: unidade, disponivel: disponivel,
                category: category, compra_minima: compra_minima, granularidade:granularidade, csrfmiddlewaretoken: csrf },
            success: function (data) {
                callback();
            },
            error: function (xhr, errmsg, err) {
                alert(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            }
        });
    };

    //noinspection JSUnusedLocalSymbols
    salva_produto = function (csrf, callback) {
        jQuery.ajax({
            type: "POST",
            cache: false,
            data: { action: 'query', tipodebusca: tipodebusca, query_string: query_string, csrfmiddlewaretoken: csrf },
            success: function (data) {
                jQueryData = jQuery.parseJSON(data);
                callback(jQueryData);
            },
            error: function (xhr, errmsg, err) {
                alert(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            }
        });
    };

});

