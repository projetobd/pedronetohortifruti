jQuery(document).ready(function(){
	var viewHeight = 300,
    	viewWidth = 700;

    //Função específica para esse botão da barra abrir o view
    abrir_view_add_produto = function(obj) {
        var cd_quick_view = $('.view');

        cd_quick_view.find('div').remove();

        var template = $('#insere-produto-template').html();

        templObj = $(template);

        cd_quick_view.append(templObj);

		var botao = obj.children('.barra_usuario_botao');

        var topSelected = botao.offset().top,
			leftSelected = botao.offset().left,
			widthSelected = botao.width(),
			heightSelected = botao.height(),
            windowWidth = $("#conteudo").width(),
            windowHeight = $("#conteudo").height(),
			finalLeft = (windowWidth - viewWidth)/2,
			finalHeight = viewHeight,
            finalTop = (windowHeight - 100) / 2,
            quickViewWidth = viewWidth,
			quickViewLeft = (windowWidth - quickViewWidth)/2;

	    $('.view').data({
	        'topSelected': topSelected,
	        'leftSelected': leftSelected,
	        'widthSelected': widthSelected,
	        'heightSelected': heightSelected,
	        'windowWidth': windowWidth,
	        'windowHeight': windowHeight,
	        'finalLeft': finalLeft,
	        'finalHeight': finalHeight,
	        'finalTop': finalTop,
	        'quickViewWidth': quickViewWidth,
	        'quickViewLeft': quickViewLeft
	    });

        $('#salvar').tooltipster({
            trigger: 'custom',
            onlyOne: false,
            position: 'top-left'
        });
	};

    abrir_view_add_cliente = function(obj) {
        var cd_quick_view = $('.view');

        cd_quick_view.find('div').remove();

        var template = $('#insere-cliente-template').html();

        templObj = $(template);

        cd_quick_view.append(templObj);

		var botao = obj.children('.barra_usuario_botao');

        var topSelected = botao.offset().top,
			leftSelected = botao.offset().left,
			widthSelected = botao.width(),
			heightSelected = botao.height(),
            windowWidth = $("#conteudo").width(),
            windowHeight = $("#conteudo").height(),
			finalLeft = (windowWidth - viewWidth)/2,
			finalHeight = 400,
            finalTop = (windowHeight - 100) / 2,
            quickViewWidth = viewWidth,
			quickViewLeft = (windowWidth - quickViewWidth)/2;

	    $('.view').data({
	        'topSelected': topSelected,
	        'leftSelected': leftSelected,
	        'widthSelected': widthSelected,
	        'heightSelected': heightSelected,
	        'windowWidth': windowWidth,
	        'windowHeight': windowHeight,
	        'finalLeft': finalLeft,
	        'finalHeight': finalHeight,
	        'finalTop': finalTop,
	        'quickViewWidth': quickViewWidth,
	        'quickViewLeft': quickViewLeft
	    });

        $('#salvar').tooltipster({
            trigger: 'custom',
            onlyOne: false,
            position: 'top-left'
        });
	};

	abrir_view_add_pedido = function(obj) {
        var cd_quick_view = $('.view');

        cd_quick_view.find('div').remove();

        var template = $('#insere_pedido_template').html();

        templObj = $(template);

        cd_quick_view.append(templObj);

		var botao = obj.children('.barra_usuario_botao');

        var topSelected = botao.offset().top,
			leftSelected = botao.offset().left,
			widthSelected = botao.width(),
			heightSelected = botao.height(),
            windowWidth = $("#conteudo").width(),
            windowHeight = $("#conteudo").height(),
			finalLeft = (windowWidth - viewWidth)/2,
			finalHeight = 450,
            finalWidth = 1500,
            finalTop = (windowHeight - 100) / 4,
            quickViewWidth = 1500,
			quickViewLeft = (windowWidth - quickViewWidth)/2;

	    $('.view').data({
	        'topSelected': topSelected,
	        'leftSelected': leftSelected,
	        'widthSelected': widthSelected,
	        'heightSelected': heightSelected,
	        'windowWidth': windowWidth,
	        'windowHeight': windowHeight,
	        'finalLeft': finalLeft,
	        'finalHeight': finalHeight,
            'finalWidth': finalWidth,
	        'finalTop': finalTop,
	        'quickViewWidth': quickViewWidth,
	        'quickViewLeft': quickViewLeft
	    });

        $('#salvar').tooltipster({
            trigger: 'custom',
            onlyOne: false,
            position: 'top-left'
        });

        init_searches();
	};

    //Atribui as funções ao botão
    preparar_abrir_view($('.insere_produto'), abrir_view_add_produto);
    preparar_abrir_view($('.insere_produto'), abrir_view_generico);

    preparar_abrir_view($('.insere_cliente'), abrir_view_add_cliente);
    preparar_abrir_view($('.insere_cliente'), abrir_view_generico);

    preparar_abrir_view($('.insere_pedido'), abrir_view_add_pedido);
    preparar_abrir_view($('.insere_pedido'), abrir_view_generico);

	function animateInsertView(botao) {

		//place the quick view over the image gallery and give it the dimension of the gallery image
		$('.view').css({
		    "top": topSelected,
		    "left": leftSelected,
		    "width": widthSelected,
			"height": heightSelected,
            "opacity": 0
		}).velocity({
			//animate the quick view: animate its width and center it in the viewport
			//during this animation, only the slider image is visible
		    'top': finalTop+ 'px',
		    'left': finalLeft+'px',
		    'width': viewWidth+'px',
			'height': finalHeight+'px',
            'opacity': 1
		}, 500, [ 40, 20 ], function() {
			//animate the quick view: animate its width to the final value
			$('.view').addClass('animate-width').velocity({
				'left': quickViewLeft+'px',
		    	'width': quickViewWidth+'px',
			}, 300, 'ease' ,function(){
				//show quick view content
				$('.view').addClass('add-content');
			});
		}).addClass('is-visible');
	}

	function closeNoAnimation(image, finalWidth, maxQuickWidth) {
		var parentListItem = image.parent('.cd-item'),
			topSelected = image.offset().top - $(this).parent().scrollTop(),
			leftSelected = image.offset().left,
			widthSelected = image.width();

		//close the quick view reverting the animation
		$('body').removeClass('overlay-layer');
		parentListItem.removeClass('empty-box');
		$('.view').velocity("stop").removeClass('add-content animate-width is-visible').css({
			"top": topSelected,
		    "left": leftSelected,
		    "width": widthSelected,
		});
	}
});
