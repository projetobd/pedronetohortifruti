from django.conf.urls import patterns, include, url
from django.contrib import admin

from website import views


urlpatterns = patterns('',
                       url(r'^$', views.index, name='index'),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'index_home.html'}),
                       url(r'^logout/$', views.logout_index, name='logout_home'),
                       url(r'^website/', include('website.urls')),
)
