# -*- coding: utf-8 -*-
import re
import json
from django.core import serializers
from decimal import Decimal
import datetime

from django.conf import settings

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseBadRequest
from django.forms.formsets import formset_factory
from localflavor.br.forms import phone_digits_re
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import logout
from django.core.serializers.json import DjangoJSONEncoder
from sorl import thumbnail
from forms import *

from busca import get_query


def index(request):
    if request.is_ajax():
        if request.method == 'GET':
            action = request.GET.get('action')
            if action == 'query':
                return search(request)
            if action == 'random_produtos':
                return r_produtos(request)
            if action == 'random_clientes':
                return r_clientes(request)
            if action == 'cliente_pedidos':
                return cliente_pedidos(request)
            if action == 'pedido_produtos':
                return pedido_produtos(request)
        if request.method == 'POST':
            action = request.POST.get('action')
            if action == 'insert_produto':
                return insert_produto(request)
            if action == 'insert_cliente':
                return insert_cliente(request)
            if action == 'insert_pedido':
                return insert_pedido(request)
            if action == 'envia_email':
                return envia_email(request)

    authform = AuthenticationForm
    dictionary = {'form': authform}

    if request.user.is_authenticated():
        list_pessoa = Pessoa.objects.filter(user=request.user)
        if len(list_pessoa) > 0:
            pessoa = list_pessoa[0]
            list_user_image = ImagemPessoa.objects.filter(pessoa=pessoa)
            if len(list_user_image) > 0:
                dictionary['user_image'] = list_user_image[0].original

        if not dictionary.has_key('user_image'):
            dictionary['user_image'] = ImagemPadrao.objects.get(uso='user_image').imagem

    return render(request, 'index_home.html', dictionary)


def logout_index(request):
    logout(request)
    return redirect('index')


def insercoes(request):
    return render(request, 'index.html')


def insert_produto(request):
    p = Produto(
        nome=request.POST.get('nome'),
        preco=request.POST.get('preco'),
        disponivel=request.POST.get('disponivel'),
        unidade=request.POST.get('unidade'),
        category=request.POST.get('category'),
        compra_minima=request.POST.get('compra_minima'),
        granularidade=request.POST.get('granularidade'),
    )
    p.save()

    return HttpResponse(json.dumps({"status": "Success"}), content_type="application/json")


def insert_cliente(request):
    #p = Pessoa(
    #    nome=request.POST.get('first_name'),
    #)
    #p.save()

    u = User(
        username=request.POST.get('login'),
        first_name=request.POST.get('first_name'),
        last_name=request.POST.get('last_name'),
        is_active=True,
        email=request.POST.get('email')
    )
    u.save()

    p = Pessoa(
        user=u,
        url_externo=request.POST.get('url_externo')
    )
    p.save()

    e = Endereco(
        pessoa=p,
        cep=request.POST.get('cep'),
        rua=request.POST.get('rua'),
        complemento=request.POST.get('complemento'),
        numero=request.POST.get('numero_endereco'),
        bairro=request.POST.get('bairro'),
        cidade=request.POST.get('cidade'),
        estado=request.POST.get('estado')
    )
    e.save()

    t1 = Telefone(
        pessoa=p,
        ddd=request.POST.get('ddd'),
        numero=request.POST.get('numero')
    )
    t1.save()

    t2 = Telefone(
        pessoa=p,
        ddd=request.POST.get('ddd2'),
        numero=request.POST.get('numero2')
    )
    t2.save()

    return HttpResponse(json.dumps({"status": "Success"}), content_type="application/json")


def insert_pedido(request):
    print 'c'
    produtos = json.loads(request.POST.get('produtos'))
    precos = json.loads(request.POST.get('preco'))
    qtd = json.loads(request.POST.get('qtd'))
    user = request.POST.get('user')

    print precos
    print qtd
    print 'd'
    # Calcula o valor total da compra
    valor_total = 0
    valor = {}

    for prod in produtos:
        val = Decimal(precos[prod])*Decimal(qtd[prod])
        valor[prod] = val
        valor_total = valor_total + val

    p = Pedido(
        pessoa=Pessoa.objects.get(user=user),
        data_entrega=datetime(2011, 10, 1, 15, 26),
        valor_pago=0,
        valor_total=valor_total
    )
    p.save()
    print 'e'
    for prod in produtos:
        print 'A'
        print prod
        print 'B'
        prod_row = Produto.objects.get(id=prod)
        print prod_row
        ped = Pedido_Produto(
            pedido=p,
            produto=prod_row,
            preco_total=valor[prod],
            preco_produto_final=precos[prod],
            preco_produto=prod_row.preco,
            quantidade_produto=qtd[prod]
        )
        print ped
        ped.save()

    print 'f'
    return HttpResponse(json.dumps({"status": "Success"}), content_type="application/json")


def cliente_pedidos(request):
    print request.GET.get('cliente')
    print Pessoa.objects.get(user=request.GET.get('cliente'))
    print 'b'

    found_entries = Pedido.objects.filter(pessoa=Pessoa.objects.get(user=request.GET.get('cliente')))
    pedidos = json.dumps([ob.as_json() for ob in found_entries])
    print Pessoa.objects.get(id=request.GET.get('cliente'))
    return HttpResponse(json.dumps(pedidos), content_type="application/json")


def pedido_produtos(request):
    found_entries = Pedido_Produto.objects.filter(pedido=Pedido.objects.get(id=request.GET.get('pedido')))
    pedido_produtos = json.dumps([ob.as_json() for ob in found_entries])

    return HttpResponse(json.dumps(pedido_produtos), content_type="application/json")


def r_produtos(request):
    found_entries = Produto.objects.filter(disponivel=True).order_by('?')
    produtos = json.dumps([ob.as_json() for ob in found_entries])
    return HttpResponse(json.dumps(produtos), content_type="application/json")


def r_clientes(request):    
    found_entries = Pessoa.objects.filter(user__is_superuser=False, user__is_active=True).order_by('?')
    clientes = json.dumps([ob.as_json(thumbnail_size='x170',thumbnail_crop='noop') for ob in found_entries])
    return HttpResponse(json.dumps(clientes), content_type="application/json")


def envia_email(request):
	nome = request.POST.get('nome', '')
	telefone = request.POST.get('telefone', '')
	mensagem = request.POST.get('mensagem', '')
	from_email = request.POST.get('email', '')

	if nome != "" and mensagem != "" and from_email != "":
		m = Mensagem(
		    nome=nome,
		    email=from_email,
		    telefone=telefone,
		    mensagem = mensagem,
		)
		m.save()

		return HttpResponse(json.dumps({"status": "Success"}), content_type="application/json")

	else:
		return HttpResponseBadRequest(json.dumps({"success": False, "status": "error", "msg": "Campo obrigatório vazio"}), content_type="application/json")


def search(request):
    tipodebusca = request.GET.get('tipodebusca')
    if tipodebusca == 'produto':
        query_string = request.GET.get('query_string')

        if not query_string:
            found_entries = Produto.objects.order_by('nome')
            produtos = json.dumps([ob.as_json() for ob in found_entries])

        else:
            entry_query = get_query(query_string, ['nome', ])
            found_entries = Produto.objects.filter(entry_query).order_by('nome')
            produtos = json.dumps([ob.as_json() for ob in found_entries])

        return HttpResponse(json.dumps(produtos), content_type="application/json")

    elif tipodebusca == 'cliente':
        query_string = request.GET.get('query_string')

        if not query_string:
            found_entries = Pessoa.objects.filter(user__is_superuser=False).order_by('user__first_name')
            cliente = json.dumps([ob.as_json() for ob in found_entries])

        else:
            entry_query = get_query(query_string, ['user__first_name', ])
            found_entries = Pessoa.objects.filter(entry_query).filter(user__is_superuser=False).order_by('user__first_name')
            cliente = json.dumps([ob.as_json() for ob in found_entries])

        return HttpResponse(json.dumps(cliente), content_type="application/json")

    elif tipodebusca == 'pedido':
        query_string = request.GET.get('query_string')

        if not query_string:
            found_entries = Pedido.objects.order_by('observacao')
            pedido = json.dumps([ob.as_json for ob in found_entries])

            # else:
            #entry_query = get_query(query_string, ['observacao', ])
            #found_entries = Pedido.objects.filter(entry_query).order_by('observacao')
            #pedido = serializers.serialize('json', found_entries)
        else:
            #A busca foi de etata em etapa, primeiro em user, depois pessoa e por ultimo em Pedido.
            entry_query = get_query(query_string, ['user__first_name', ])
            found_user = User.objects.filter(entry_query,user__is_superuser=False)

            for i in found_user:
                if not found_pessoas:
                    found_pessoas = Pessoa.objects.filter(user=i)
                else:
                    found_pessoas.append(Pessoa.objects.filter(user=i))

            for i in found_pessoas:
                if not found_entries:
                    found_entries = Pedido.objects.filter(pessoa=i)
                else:
                    found_entries.append(Pedido.objects.filter(pessoa=i))

            #found_entries = Pedido.objects.filter(entry_query).order_by('observacao')
            pedido = json.dumps([ob.as_json() for ob in found_entries])

        return HttpResponse(json.dumps(pedido), content_type="application/json")


def cadastrar_pedido_admin(request):
    # lembrar de recalcular o valor total aqui depois do POST
    if request.is_ajax():
        action = request.POST.get('action')
        if action == 'get_preco':
            prod_id = request.POST.get('id')

        preco = Produto.objects.get(id=prod_id).preco
        response_dict = {'preco': preco}
        return HttpResponse(json.dumps(response_dict, cls=DjangoJSONEncoder), content_type="application/json")
    usuarios = []
    for usuario in Pessoa.objects.all():
        usuarios = usuarios + [(usuario, usuario.nome)]

    produtos = []
    produtos2 = []
    for produto in Produto.objects.all():
        produtos2 = produtos2 + [produto.nome]
        produtos = produtos + [(produto, produto.nome)]

    form = Pedido_Admin_Form()
    form.fields['usuario'].choices = usuarios

    # return HttpResponse(' '.join(produtos2))
    return render(request, 'cadastro/pedido_admin.html', {'form': form})


def cadastrar_produto(request):
    form = Cadastro_Produto_Form()
    if request.method == 'POST':
        form = Cadastro_Produto_Form(request.POST)
        if form.is_valid():
            p = Produto(
                nome=request.POST['nome'],
                preco=request.POST['preco'],
                disponivel=request.POST['disponivel'],
                unidade=request.POST['unidade']
            )
            p.save()
            return HttpResponse('<h1>Produto Cadastrado com Sucesso!</h1>')

    return render(request, 'cadastro/produto.html', {'form': form})


def cadastrar_pessoa(request):
    form = Cadastro_Usuario_Form()
    if request.method == 'POST':
        form = Cadastro_Usuario_Form(request.POST)

        TelefoneFormset = formset_factory(Cadastro_Usuario_Form.Telefone_Form)

        form.telefone_formset = TelefoneFormset(request.POST, prefix='telefones')
        # telefones_check = True;
        # for telefoneform in form.telefone_formset:
        # telefones_check = telefones_check and telefoneform.is_valid()

        form.telefone_formset.is_valid()
        if form.is_valid() and form.telefone_formset.is_valid():
            tipo = request.POST['tipo']
            p = Pessoa()
            if tipo == 'admin':
                p = Admin(
                    nome=request.POST['nome'],
                    login=request.POST['login'],
                    senha=request.POST['password2'],
                    email=request.POST['email'],
                    sexo=request.POST['sexo'])
            elif tipo == 'fisica':
                p = Fisica(
                    nome=request.POST['nome'],
                    login=request.POST['login'],
                    senha=request.POST['password2'],
                    email=request.POST['email'],
                    sexo=request.POST['sexo'],
                    cpf=''.join(ele for ele in request.POST['cpf'] if ele.isdigit()))
            elif tipo == 'juridica':
                p = Juridica(
                    nome=request.POST['nome'],
                    login=request.POST['login'],
                    senha=request.POST['password2'],
                    email=request.POST['email'],
                    sexo=request.POST['sexo'],
                    cnpj=''.join(ele for ele in request.POST['cnpj'] if ele.isdigit()))

            p.save()

            for telefoneform in form.telefone_formset:
                if telefoneform.is_valid() and telefoneform.cleaned_data.has_key('telefone'):
                    m = phone_digits_re.search(telefoneform.cleaned_data['telefone'])
                    t = Telefone(pessoa=p, ddd=m.group(1), numero=''.join((m.group(2), m.group(3))))
                    t.save()
            cep_re = re.compile(r'^(\d{5})-(\d{3})$')
            m = cep_re.search(request.POST['cep'])

            e = Endereco(
                pessoa=p,
                estado=request.POST['estado'],
                cep=''.join((m.group(1), m.group(2))),
                rua=request.POST['rua'],
                numero=request.POST['numero'],
                bairro=request.POST['bairro'],
                cidade=request.POST['cidade'],
                complemento=request.POST['complemento']
            )
            e.save()

            return HttpResponse('<h1>Usuário Cadastrado com Sucesso!</h1>')

    return render(request, 'cadastro/cliente.html', {'form': form})
