from django.contrib import admin

from website.models import *
from sorl import thumbnail

admin.site.register(Produto)
admin.site.register(Pessoa)
admin.site.register(Telefone)
admin.site.register(Endereco)
admin.site.register(Pedido)
admin.site.register(Pedido_Produto)
admin.site.register(ImagemPessoa)
admin.site.register(ImagemProduto)
admin.site.register(ImagemPadrao)
admin.site.register(Mensagem)
