# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0002_pessoa_url_externo'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='telefone',
            unique_together=set([('ddd', 'numero')]),
        ),
    ]
