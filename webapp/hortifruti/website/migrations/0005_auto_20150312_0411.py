# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0004_mensagem'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mensagem',
            name='telefone',
            field=models.CharField(max_length=200, blank=True),
        ),
    ]
