# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ImagemPadrao',
            fields=[
                ('uso', models.TextField(max_length=20, serialize=False, primary_key=True)),
                ('imagem', models.ImageField(upload_to=b'padrao')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Pedido',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data_pedido', models.DateTimeField(auto_now_add=True)),
                ('data_entrega', models.DateTimeField()),
                ('valor_total', models.DecimalField(max_digits=19, decimal_places=2)),
                ('valor_pago', models.DecimalField(default=0.0, max_digits=19, decimal_places=2)),
                ('cancelado', models.BooleanField(default=False)),
                ('entregue', models.BooleanField(default=False)),
                ('pago', models.BooleanField(default=False)),
                ('observacao', models.CharField(default=b'', max_length=200, blank=True)),
                ('ultima_modificacao', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['data_pedido'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Pedido_Produto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('preco_total', models.DecimalField(max_digits=19, decimal_places=2)),
                ('preco_produto', models.DecimalField(max_digits=19, decimal_places=2)),
                ('preco_produto_final', models.DecimalField(max_digits=19, decimal_places=2)),
                ('quantidade_produto', models.DecimalField(max_digits=19, decimal_places=2)),
                ('pedido', models.ForeignKey(to='website.Pedido')),
            ],
            options={
                'ordering': ['pedido', 'produto'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Pessoa',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ultima_modificacao', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ImagemPessoa',
            fields=[
                ('pessoa', models.OneToOneField(primary_key=True, serialize=False, to='website.Pessoa')),
                ('original', models.ImageField(upload_to=b'pessoa')),
                ('criado', models.DateTimeField(auto_now_add=True)),
                ('ultima_modificacao', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Endereco',
            fields=[
                ('pessoa', models.OneToOneField(primary_key=True, serialize=False, to='website.Pessoa')),
                ('cep', models.CharField(max_length=8)),
                ('rua', models.CharField(max_length=200)),
                ('numero', models.IntegerField()),
                ('bairro', models.CharField(max_length=50)),
                ('cidade', models.CharField(default=b'Recife', max_length=50)),
                ('estado', models.CharField(default=b'Pernambuco', max_length=50)),
                ('complemento', models.CharField(max_length=200, blank=True)),
            ],
            options={
                'ordering': ['pessoa'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Produto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=50)),
                ('preco', models.DecimalField(max_digits=19, decimal_places=2)),
                ('unidade', models.CharField(default=b'KG', max_length=3, choices=[(b'KG', b'Quilograma'), (b'CX', b'Caixa'), (b'BD', b'Balde'), (b'ML', b'Molho'), (b'UND', b'Unidade')])),
                ('category', models.CharField(max_length=2, choices=[(b'F', b'Frutas'), (b'LE', b'Legumes'), (b'V', b'Verduras'), (b'C', b'Carnes'), (b'LA', b'Latic\xc3\xadnios'), (b'T', b'Temperos'), (b'UT', b'Utilidades'), (b'O', b'Outros')])),
                ('criado', models.DateTimeField(default=django.utils.timezone.now, auto_now_add=True)),
                ('ultima_modificacao', models.DateTimeField(default=django.utils.timezone.now, auto_now=True)),
                ('compra_minima', models.DecimalField(default=b'0.00', max_digits=19, decimal_places=2)),
                ('granularidade', models.DecimalField(default=b'0.00', max_digits=19, decimal_places=2)),
                ('disponivel', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ImagemProduto',
            fields=[
                ('produto', models.OneToOneField(primary_key=True, serialize=False, to='website.Produto')),
                ('original', models.ImageField(upload_to=b'produto')),
                ('criado', models.DateTimeField(auto_now_add=True)),
                ('ultima_modificacao', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Telefone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ddd', models.CharField(max_length=3)),
                ('numero', models.CharField(max_length=9)),
                ('pessoa', models.ForeignKey(to='website.Pessoa')),
            ],
            options={
                'ordering': ['ddd', 'numero'],
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='telefone',
            unique_together=set([('pessoa', 'ddd', 'numero')]),
        ),
        migrations.AddField(
            model_name='pessoa',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='pedido_produto',
            name='produto',
            field=models.ForeignKey(to='website.Produto'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='pedido',
            name='pessoa',
            field=models.ForeignKey(to='website.Pessoa'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='pedido',
            name='produtos',
            field=models.ManyToManyField(to='website.Produto', through='website.Pedido_Produto'),
            preserve_default=True,
        ),
    ]
