# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models
from django.utils import timezone

from django.contrib.auth.models import User
from sorl.thumbnail import get_thumbnail

def date_time_field_to_str(date):
    string = str(date.day) + '/' + str(date.month) + '/' + str(date.year) + ' ' + str(date.hour) + ':' + str(date.minute)
    return string

class Mensagem(models.Model):
	nome = models.CharField(max_length=200, blank=False, default='')
	email = models.CharField(max_length=200, blank=False, default='')
	telefone = models.CharField(max_length=200, blank=True)
	mensagem = models.TextField(blank=False)

class Pessoa(models.Model):
    user = models.OneToOneField(User)
    url_externo = models.URLField(default='') 
    ultima_modificacao = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.user.first_name

    def as_json(self,thumbnail_size='267x300',thumbnail_crop='center'):
        d = dict(first_name=self.user.first_name,
                 last_name=self.user.last_name,
                 email=self.user.email,
                 ativo=self.user.is_active,
                 login=self.user.username,
                 id=self.id,
                 user=self.user.id,
                 url_externo = self.url_externo,
        )
        list_telefone = Telefone.objects.filter(pessoa=self)
        if len(list_telefone) > 0:
            d['ddd'] = list_telefone[0].ddd
            d['numero'] = list_telefone[0].numero
        if len(list_telefone) > 1:
            d['ddd2'] = list_telefone[1].ddd
            d['numero2'] = list_telefone[1].numero

        list_endereco = Endereco.objects.filter(pessoa=self)
        if len(list_endereco) > 0:
            d['cep'] = list_endereco[0].cep
            d['rua'] = list_endereco[0].rua
            d['numero_endereco'] = list_endereco[0].numero
            d['bairro'] = list_endereco[0].bairro
            d['cidade'] = list_endereco[0].cidade
            d['estado'] = list_endereco[0].estado
            d['complemento'] = list_endereco[0].complemento

        list_minha_imagem = ImagemPessoa.objects.filter(pessoa=self)
        if len(list_minha_imagem) > 0:
            d['url_imagem'] = get_thumbnail(list_minha_imagem[0].original,thumbnail_size,crop=thumbnail_crop).url
            d['tem_imagem'] = True        
        else:
            d['url_imagem'] = get_thumbnail(ImagemPadrao.objects.get(uso='user_image').imagem,thumbnail_size,crop=thumbnail_crop).url
            d['tem_imagem'] = False
        return d

#User.objects.create_superuser('gabriel', 'abc@123.com', 'abc123')
#from django.contrib.auth.models import User
class Telefone(models.Model):
    class Meta:
        ordering = ['ddd', 'numero']
        unique_together = 'ddd', 'numero'

    pessoa = models.ForeignKey('Pessoa')
    ddd = models.CharField(max_length=3)
    numero = models.CharField(max_length=9)

    def __unicode__(self):
        return ('(' + self.ddd + ')' + self.numero)

class Endereco(models.Model):
    class Meta:
        ordering = ['pessoa']

    pessoa = models.OneToOneField('Pessoa', primary_key=True)
    cep = models.CharField(max_length=8)
    rua = models.CharField(max_length=200)
    numero = models.IntegerField()
    bairro = models.CharField(max_length=50)
    cidade = models.CharField(max_length=50, default='Recife')
    estado = models.CharField(max_length=50, default='Pernambuco')
    complemento = models.CharField(max_length=200, blank=True)

class Produto(models.Model):
    nome = models.CharField(max_length=50)
    preco = models.DecimalField(max_digits=19, decimal_places=2)
    UNIDADE_CHOICES = (
        ('KG', 'Quilograma'),
        ('CX', 'Caixa'),
        ('BD', 'Balde'),
        ('ML', 'Molho'),
        ('UND', 'Unidade'),
    )
    unidade = models.CharField(max_length=3, choices=UNIDADE_CHOICES, default='KG')
    CATEGORY_CHOICES = (
        ('F', 'Frutas'),
        ('LE', 'Legumes'),
        ('V', 'Verduras'),
        ('C', 'Carnes'),
        ('LA', 'Laticínios'),
        ('T', 'Temperos'),
        ('UT', 'Utilidades'),
        ('O', 'Outros'),
    )
    category = models.CharField(max_length=2, choices=CATEGORY_CHOICES)

    criado = models.DateTimeField(auto_now_add=True,default=timezone.now)
    ultima_modificacao = models.DateTimeField(auto_now=True,default=timezone.now)
    compra_minima = models.DecimalField(max_digits=19, decimal_places=2,default='0.00')
    granularidade = models.DecimalField(max_digits=19, decimal_places=2,default='0.00')
    disponivel = models.BooleanField(default=True)
    #As compras validas são nas quantidades x:
    #se granularidade == 0: x >= compra_minima
    #se não :  x = compra_minima + granularidade*d, onde d = 0,1,2..

    def __unicode__(self):
        return self.nome

    def as_json(self,thumbnail_size='267x300',thumbnail_crop='center'):
        d = dict(
            nome=unicode(self.nome),
            preco=str(self.preco),
            unidade=str(self.unidade),
            category=str(self.category),
            criado=date_time_field_to_str(self.criado),
            ultima_modificacao=date_time_field_to_str(self.ultima_modificacao),
            compra_minima=str(self.compra_minima),
            granularidade=str(self.granularidade),
            disponivel=self.disponivel,
            id=str(self.id)
        )

        list_minha_imagem = ImagemProduto.objects.filter(produto=self)
        if len(list_minha_imagem) > 0:
            d['url_imagem'] = get_thumbnail(list_minha_imagem[0].original,thumbnail_size,crop=thumbnail_crop).url
        else:
            d['url_imagem'] = get_thumbnail(ImagemPadrao.objects.get(uso='produto').imagem,thumbnail_size,crop=thumbnail_crop).url
        return d

class Pedido(models.Model):
    class Meta:
        ordering = ['data_pedido']

    pessoa = models.ForeignKey('Pessoa')

    produtos = models.ManyToManyField('Produto', through='Pedido_Produto')

    data_pedido = models.DateTimeField(auto_now_add=True)
    data_entrega = models.DateTimeField()
    valor_total = models.DecimalField(max_digits=19, decimal_places=2)
    valor_pago = models.DecimalField(default=0.00, max_digits=19, decimal_places=2)
    cancelado = models.BooleanField(default=False)
    entregue = models.BooleanField(default=False)
    pago = models.BooleanField(default=False)
    observacao = models.CharField(max_length=200, blank=True, default='')

    ultima_modificacao = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.pessoa.user.first_name + ' ' + str(self.id)

    def as_json(self,thumbnail_size='267x300',thumbnail_crop='center'):
        d = dict(pessoa_first_name=self.pessoa.user.first_name,
                    data_pedido=str(self.data_pedido),
                    data_entrega=str(self.data_entrega),
                    valor_total=str(self.valor_total),
                    valor_pago=str(self.valor_pago),
                    cancelado=self.cancelado,
                    entregue=self.entregue,
                    pago=self.pago,
                    obervacao=self.observacao,
                    id=self.id
        )
        list_minha_imagem = ImagemPessoa.objects.filter(pessoa=self.pessoa)
        if len(list_minha_imagem) > 0:
            d['url_imagem'] = get_thumbnail(list_minha_imagem[0].original,thumbnail_size,crop=thumbnail_crop).url
        else:
            d['url_imagem'] = get_thumbnail(ImagemPadrao.objects.get(uso='user_image').imagem,thumbnail_size,crop=thumbnail_crop).url
        return d

class Pedido_Produto(models.Model):
    class Meta:
        ordering = ['pedido', 'produto']

    pedido = models.ForeignKey('Pedido')
    produto = models.ForeignKey('Produto')
    preco_total = models.DecimalField(max_digits=19,
                                              decimal_places=2)  # automatico quantidade_produto*preco_unidade_produto - desconto*qtd
    preco_produto = models.DecimalField(max_digits=19,
                                                decimal_places=2)
    preco_produto_final = models.DecimalField(max_digits=19, decimal_places=2)
    quantidade_produto = models.DecimalField(max_digits=19, decimal_places=2)  # check maior que 0

    def __unicode__(self):
        return (self.pedido.pessoa.user.first_name + ' ' + str(self.pedido.id) + ' | ' + self.produto.nome + ' ' + str(
            self.produto.id) + ' | ' + str(self.id))

    def as_json(self):
        return dict(
            produto=self.produto.id,
            produto_nome=self.produto.nome,
            produto_und=self.produto.unidade,
            preco=str(self.preco_produto),
            precoF=str(self.preco_produto_final),
            qtd=str(self.quantidade_produto),
            total=str(self.preco_total),
            id=self.id
        )

class ImagemPessoa(models.Model):
    pessoa = models.OneToOneField('Pessoa', primary_key=True)
    original = models.ImageField(blank=False,upload_to='pessoa')
    criado = models.DateTimeField(auto_now_add=True)
    ultima_modificacao = models.DateTimeField(auto_now=True)

class ImagemProduto(models.Model):
    produto = models.OneToOneField('Produto', primary_key=True)
    original = models.ImageField(blank=False,upload_to='produto')
    criado = models.DateTimeField(auto_now_add=True)
    ultima_modificacao = models.DateTimeField(auto_now=True)

class ImagemPadrao(models.Model):
    uso = models.TextField(primary_key=True,max_length=20)
    imagem = models.ImageField(blank=False,upload_to='padrao')
