from django.conf.urls import patterns, url

from website import views
from hortifruti import settings

urlpatterns = patterns('',
                       url(r'^$', views.index, name='website_index'),
                       url(r'^insercoes$', views.insercoes, name='insercoes'),
                       url(r'^cadastrar_pessoa/', views.cadastrar_pessoa, name='cadastrar_pessoa'),
                       url(r'^cadastrar_produto/', views.cadastrar_produto, name='cadastrar_produto'),
                       url(r'^cadastrar_pedido_admin/', views.cadastrar_pedido_admin, name='cadastrar_pedido_admin'),

                       url(r'^media/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.MEDIA_ROOT,'show_indexes': True}),#DEBUG
)
