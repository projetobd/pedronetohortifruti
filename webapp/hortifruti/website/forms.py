# -*- coding: utf-8 -*-
from django.utils.translation import gettext as _
from localflavor.br.forms import BRCPFField, BRCNPJField, BRPhoneNumberField, BRZipCodeField, BRStateChoiceField
from django.forms.formsets import formset_factory
from django import forms
from django.contrib.admin.widgets import AdminDateWidget

from models import *


class Pedido_Produto_Form(forms.Form):
    desconto = forms.DecimalField(max_digits=19, decimal_places=2)
    quantidade = forms.DecimalField(max_digits=19, decimal_places=2)
    preco = forms.DecimalField(max_digits=19, decimal_places=2, widget=forms.TextInput(attrs={'readonly': 'readonly'}))

    def __init__(self, *args, **kwargs):
        super(Pedido_Produto_Form, self).__init__(*args, **kwargs)
        produtos = [('', '-----')]
        for produto in Produto.objects.all():
            produtos = produtos + [(produto.id, produto.nome)]
        self.fields['produto'] = forms.ChoiceField(widget=forms.Select(), choices=produtos)


class Pedido_Admin_Form(forms.Form):
    usuario = forms.ChoiceField()

    data_entrega = forms.DateField(widget=AdminDateWidget)

    valor_total = forms.DecimalField(max_digits=19, decimal_places=2,
                                     widget=forms.TextInput(attrs={'readonly': 'readonly'}))

    valor_pago = forms.DecimalField(max_digits=19, decimal_places=2)

    cancelado = forms.BooleanField(required=False)

    entregue = forms.BooleanField(required=False)

    pago = forms.BooleanField(required=False)

    observacao = forms.CharField(max_length=200, required=False)

    PedidoProdutoFormset = formset_factory(Pedido_Produto_Form)

    pedido_produto_formset = PedidoProdutoFormset(prefix='pedidoprodutos')


class Cadastro_Produto_Form(forms.Form):
    nome = forms.CharField(max_length=50)
    preco = forms.DecimalField(max_digits=19, decimal_places=2)
    disponivel = forms.BooleanField(required=False)
    UNIDADE = [('KG', 'Quilograma'), ('CX', 'Caixa'), ('BD', 'Balde'), ('ML', 'Molho'), ('UND', 'Unidade')]
    unidade = forms.ChoiceField(choices=UNIDADE, widget=forms.RadioSelect())


class Cadastro_Usuario_Form(forms.Form):
    error_messages = {
        'duplicate_username': _("Um usuário com esse nome já existe."),
        'password_mismatch': _("Os dois campos de senha não estão iguais."),
    }

    login = forms.RegexField(label=_("Login"), max_length=20,
                             regex=r'^[\w.@+-]+$',
                             help_text=_("<br /><br />"),
                             error_messages={'invalid': _("Este campo contém letras, números e @/./+/-/_ apenas."),
                                             'required': "Por favor preencha este campo."})

    password1 = forms.CharField(label=_("Senha"), error_messages={'required': "Por favor preencha este campo."},
                                widget=forms.PasswordInput)

    password2 = forms.CharField(label=_("Confirmação de Senha"), widget=forms.PasswordInput,
                                error_messages={'required': "Por favor preencha este campo."})

    nome = forms.CharField(label=_("Nome"), max_length=100,
                           error_messages={'required': "Por favor preencha este campo."})

    email = forms.EmailField(label=_("Email"), max_length=200, required=False)

    CHOICE = [('M', 'Masculino'), ('F', 'Feminino'), ('ND', 'Não definido')]
    sexo = forms.ChoiceField(choices=CHOICE, widget=forms.RadioSelect(),
                             error_messages={'required': "Por favor preencha este campo."})

    TIPO = [('fisica', 'Pessoa Física'), ('juridica', 'Pessoa Jurídica'), ('admin', 'Administrador')]
    tipo = forms.ChoiceField(choices=TIPO, widget=forms.RadioSelect(),
                             error_messages={'required': "Por favor preencha este campo."})

    cpf = BRCPFField(label=_("CPF"), required=False)

    cnpj = BRCNPJField(label=_("CNPJ"), required=False)

    class Telefone_Form(forms.Form):
        telefone = BRPhoneNumberField(label=_("telefone"), required=False, error_messages={
            'invalid': _(('Números de telefone devem estar no seguinte formato: XX-XXXX-XXXX ou XX-XXXXX-XXXX.')), })

    TelefoneFormset = formset_factory(Telefone_Form)

    telefone_formset = TelefoneFormset(prefix='telefones')

    estado = BRStateChoiceField()

    cidade = forms.CharField(max_length=50)

    cep = BRZipCodeField(max_length=9)

    rua = forms.CharField(max_length=200)

    numero = forms.IntegerField()

    bairro = forms.CharField(max_length=50)

    complemento = forms.CharField(max_length=200, required=False)

    class Meta:
        model = Pessoa
        fields = ("login", "password1", "password2", "nome", "email", "sexo", "tipo")

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2
